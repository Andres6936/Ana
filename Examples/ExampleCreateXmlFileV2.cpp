#include "Ana.hpp"

using namespace Ana;

int main(int argc, char** argv)
{
	// same as ExampleCreateXmlFile.cpp but add each node
	// as early as possible into the tree.

	Document doc;
	Declaration* decl = new Declaration("1.0", "", "");
	doc.LinkEndChild(decl);

	Element* element = new Element("Hello");
	doc.LinkEndChild(element);

	Text* text = new Text("World");
	element->LinkEndChild(text);

	doc.SaveFile("MadeByHand2.xml");
}