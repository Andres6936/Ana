/*
www.sourceforge.net/projects/tinyxml
Original code (2.0 and earlier )copyright (c) 2000-2006 Lee Thomason (www.grinninglizard.com)

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any
damages arising from the use of this software.

Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and
redistribute it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must
not claim that you wrote the original software. If you use this
software in a product, an acknowledgment in the product documentation
would be appreciated but is not required.

2. Altered source versions must be plainly marked as such, and
must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any source
distribution.
*/

#include "Ana.hpp"
#include "AnaPrinter.hpp"

#include <cctype>
#include <sstream>
#include <iostream>

using namespace Ana;

bool TiXmlBase::condenseWhiteSpace = true;

// Microsoft compiler security
FILE* TiXmlFOpen(const char* filename, const char* mode)
{
#if defined(_MSC_VER) && (_MSC_VER >= 1400)
	FILE* fp = 0;
	errno_t err = fopen_s( &fp, filename, mode );
	if ( !err && fp )
		return fp;
	return 0;
#else
	return fopen(filename, mode);
#endif
}

void TiXmlBase::EncodeString(const std::string& str, std::string* outString)
{
	int i = 0;

	while (i < (int)str.length())
	{
		unsigned char c = (unsigned char)str[i];

		if (c == '&'
			&& i < ((int)str.length() - 2)
			&& str[i + 1] == '#'
			&& str[i + 2] == 'x')
		{
			// Hexadecimal character reference.
			// Pass through unchanged.
			// &#xA9;	-- copyright symbol, for example.
			//
			// The -1 is a bug fix from Rob Laveaux. It keeps
			// an overflow from happening if there is no ';'.
			// There are actually 2 ways to exit this loop -
			// while fails (error case) and break (semicolon found).
			// However, there is no mechanism (currently) for
			// this function to return an error.
			while (i < (int)str.length() - 1)
			{
				outString->append(str.c_str() + i, 1);
				++i;
				if (str[i] == ';')
				{
					break;
				}
			}
		}
		else if (c == '&')
		{
			outString->append(entity[0].str, entity[0].strLength);
			++i;
		}
		else if (c == '<')
		{
			outString->append(entity[1].str, entity[1].strLength);
			++i;
		}
		else if (c == '>')
		{
			outString->append(entity[2].str, entity[2].strLength);
			++i;
		}
		else if (c == '\"')
		{
			outString->append(entity[3].str, entity[3].strLength);
			++i;
		}
		else if (c == '\'')
		{
			outString->append(entity[4].str, entity[4].strLength);
			++i;
		}
		else if (c < 32)
		{
			// Easy pass at non-alpha/numeric/symbol
			// Below 32 is symbolic.
			char buf[32];

#if defined(TIXML_SNPRINTF)
			TIXML_SNPRINTF(buf, sizeof(buf), "&#x%02X;", (unsigned)(c & 0xff));
#else
			sprintf( buf, "&#x%02X;", (unsigned) ( c & 0xff ) );
#endif

			//*ME:	warning C4267: convert 'size_t' to 'int'
			//*ME:	Int-Cast to make compiler happy ...
			outString->append(buf, (int)strlen(buf));
			++i;
		}
		else
		{
			//char realc = (char) c;
			//outString->append( &realc, 1 );
			*outString += (char)c;    // somewhat more efficient function call.
			++i;
		}
	}
}

TiXmlNode::TiXmlNode(NodeType _type)
{
	parent = nullptr;
	type = _type;
	firstChild = nullptr;
	lastChild = nullptr;
	prev = nullptr;
	next = nullptr;
}


TiXmlNode::~TiXmlNode()
{
	TiXmlNode* node = firstChild;
	TiXmlNode* temp = 0;

	while (node)
	{
		temp = node;
		node = node->next;
		delete temp;
	}
}


void TiXmlNode::CopyTo(TiXmlNode* target) const
{
	target->SetValue(value.c_str());
	target->userData = userData;
}


void TiXmlNode::Clear()
{
	TiXmlNode* node = firstChild;
	TiXmlNode* temp = 0;

	while (node)
	{
		temp = node;
		node = node->next;
		delete temp;
	}

	firstChild = 0;
	lastChild = 0;
}


TiXmlNode* TiXmlNode::LinkEndChild(TiXmlNode* node)
{
	assert(node->parent == nullptr || node->parent == this);
	assert(node->GetDocument() == nullptr || node->GetDocument() == this->GetDocument());

	if (node->Type() == TiXmlNode::DOCUMENT)
	{
		delete node;
		if (GetDocument())
		{ GetDocument()->SetError(TIXML_ERROR_DOCUMENT_TOP_ONLY, 0, 0, Encoding::Unknow); }
		return nullptr;
	}

	node->parent = this;

	node->prev = lastChild;
	node->next = nullptr;

	if (lastChild)
	{
		lastChild->next = node;
	}
	else
	{
		firstChild = node;
	}            // it was an empty list.

	lastChild = node;
	return node;
}


TiXmlNode* TiXmlNode::InsertEndChild(const TiXmlNode& addThis)
{
	if (addThis.Type() == TiXmlNode::DOCUMENT)
	{
		if (GetDocument())
		{ GetDocument()->SetError(TIXML_ERROR_DOCUMENT_TOP_ONLY, 0, 0, Encoding::Unknow); }
		return 0;
	}
	TiXmlNode* node = addThis.Clone();
	if (!node)
	{
		return 0;
	}

	return LinkEndChild(node);
}


TiXmlNode* TiXmlNode::InsertBeforeChild(TiXmlNode* beforeThis, const TiXmlNode& addThis)
{
	if (!beforeThis || beforeThis->parent != this)
	{
		return 0;
	}
	if (addThis.Type() == TiXmlNode::DOCUMENT)
	{
		if (GetDocument())
		{ GetDocument()->SetError(TIXML_ERROR_DOCUMENT_TOP_ONLY, 0, 0, Encoding::Unknow); }
		return 0;
	}

	TiXmlNode* node = addThis.Clone();
	if (!node)
	{
		return 0;
	}
	node->parent = this;

	node->next = beforeThis;
	node->prev = beforeThis->prev;
	if (beforeThis->prev)
	{
		beforeThis->prev->next = node;
	}
	else
	{
		assert(firstChild == beforeThis);
		firstChild = node;
	}
	beforeThis->prev = node;
	return node;
}


TiXmlNode* TiXmlNode::InsertAfterChild(TiXmlNode* afterThis, const TiXmlNode& addThis)
{
	if (!afterThis || afterThis->parent != this)
	{
		return 0;
	}
	if (addThis.Type() == TiXmlNode::DOCUMENT)
	{
		if (GetDocument())
		{ GetDocument()->SetError(TIXML_ERROR_DOCUMENT_TOP_ONLY, 0, 0, Encoding::Unknow); }
		return 0;
	}

	TiXmlNode* node = addThis.Clone();
	if (!node)
	{
		return 0;
	}
	node->parent = this;

	node->prev = afterThis;
	node->next = afterThis->next;
	if (afterThis->next)
	{
		afterThis->next->prev = node;
	}
	else
	{
		assert(lastChild == afterThis);
		lastChild = node;
	}
	afterThis->next = node;
	return node;
}


TiXmlNode* TiXmlNode::ReplaceChild(TiXmlNode* replaceThis, const TiXmlNode& withThis)
{
	if (replaceThis->parent != this)
	{
		return 0;
	}

	TiXmlNode* node = withThis.Clone();
	if (!node)
	{
		return 0;
	}

	node->next = replaceThis->next;
	node->prev = replaceThis->prev;

	if (replaceThis->next)
	{
		replaceThis->next->prev = node;
	}
	else
	{
		lastChild = node;
	}

	if (replaceThis->prev)
	{
		replaceThis->prev->next = node;
	}
	else
	{
		firstChild = node;
	}

	delete replaceThis;
	node->parent = this;
	return node;
}


bool TiXmlNode::RemoveChild(TiXmlNode* removeThis)
{
	if (removeThis->parent != this)
	{
		assert(0);
		return false;
	}

	if (removeThis->next)
	{
		removeThis->next->prev = removeThis->prev;
	}
	else
	{
		lastChild = removeThis->prev;
	}

	if (removeThis->prev)
	{
		removeThis->prev->next = removeThis->next;
	}
	else
	{
		firstChild = removeThis->next;
	}

	delete removeThis;
	return true;
}

const TiXmlNode* TiXmlNode::FirstChild(const char* _value) const
{
	const TiXmlNode* node;
	for (node = firstChild; node; node = node->next)
	{
		if (strcmp(node->GetValue(), _value) == 0)
		{
			return node;
		}
	}
	return 0;
}


const TiXmlNode* TiXmlNode::LastChild(const char* _value) const
{
	const TiXmlNode* node;
	for (node = lastChild; node; node = node->prev)
	{
		if (strcmp(node->GetValue(), _value) == 0)
		{
			return node;
		}
	}
	return 0;
}


const TiXmlNode* TiXmlNode::IterateChildren(const TiXmlNode* previous) const
{
	if (!previous)
	{
		return FirstChild();
	}
	else
	{
		assert(previous->parent == this);
		return previous->NextSibling();
	}
}


const TiXmlNode* TiXmlNode::IterateChildren(const char* val, const TiXmlNode* previous) const
{
	if (!previous)
	{
		return FirstChild(val);
	}
	else
	{
		assert(previous->parent == this);
		return previous->NextSibling(val);
	}
}


const TiXmlNode* TiXmlNode::NextSibling(const char* _value) const
{
	const TiXmlNode* node;
	for (node = next; node; node = node->next)
	{
		if (strcmp(node->GetValue(), _value) == 0)
		{
			return node;
		}
	}
	return 0;
}


const TiXmlNode* TiXmlNode::PreviousSibling(const char* _value) const
{
	const TiXmlNode* node;
	for (node = prev; node; node = node->prev)
	{
		if (strcmp(node->GetValue(), _value) == 0)
		{
			return node;
		}
	}
	return 0;
}


void Element::RemoveAttribute(const char* name)
{
	std::string str(name);

	Attribute* node = attributeSet.Find(str);

	if (node)
	{
		attributeSet.Remove(node);
		delete node;
	}
}

const Element* TiXmlNode::FirstChildElement() const
{
	const TiXmlNode* node;

	for (node = FirstChild();
		 node;
		 node = node->NextSibling())
	{
		if (node->ToElement())
		{
			return node->ToElement();
		}
	}
	return 0;
}


const Element* TiXmlNode::FirstChildElement(const char* _value) const
{
	const TiXmlNode* node;

	for (node = FirstChild(_value);
		 node;
		 node = node->NextSibling(_value))
	{
		if (node->ToElement())
		{
			return node->ToElement();
		}
	}
	return 0;
}


const Element* TiXmlNode::NextSiblingElement() const
{
	const TiXmlNode* node;

	for (node = NextSibling();
		 node;
		 node = node->NextSibling())
	{
		if (node->ToElement())
		{
			return node->ToElement();
		}
	}
	return 0;
}


const Element* TiXmlNode::NextSiblingElement(const char* _value) const
{
	const TiXmlNode* node;

	for (node = NextSibling(_value);
		 node;
		 node = node->NextSibling(_value))
	{
		if (node->ToElement())
		{
			return node->ToElement();
		}
	}
	return 0;
}


const Document* TiXmlNode::GetDocument() const
{
	const TiXmlNode* node;

	for (node = this; node; node = node->parent)
	{
		if (node->ToDocument())
		{
			return node->ToDocument();
		}
	}
	return 0;
}


Element::Element(const char* _value)
		: TiXmlNode(TiXmlNode::ELEMENT)
{
	firstChild = lastChild = nullptr;
	value = _value;
}

Element::Element(const std::string& _value)
		: TiXmlNode(TiXmlNode::ELEMENT)
{
	firstChild = lastChild = 0;
	value = _value;
}


Element::Element(const Element& copy)
		: TiXmlNode(TiXmlNode::ELEMENT)
{
	firstChild = lastChild = 0;
	copy.CopyTo(this);
}


void Element::operator=(const Element& base)
{
	ClearThis();
	base.CopyTo(this);
}


Element::~Element()
{
	ClearThis();
}


void Element::ClearThis()
{
	Clear();
	while (attributeSet.First())
	{
		Attribute* node = attributeSet.First();
		attributeSet.Remove(node);
		delete node;
	}
}


const char* Element::GetAttribute(const char* name) const
{
	const Attribute* node = attributeSet.Find(name);
	if (node)
	{
		return node->Value();
	}
	return 0;
}


const std::string* Element::GetAttribute(const std::string& name) const
{
	const Attribute* node = attributeSet.Find(name);
	if (node)
	{
		return &node->ValueStr();
	}
	return 0;
}


const char* Element::GetAttribute(const char* name, int* i) const
{
	const char* s = GetAttribute(name);
	if (i)
	{
		if (s)
		{
			*i = atoi(s);
		}
		else
		{
			*i = 0;
		}
	}
	return s;
}


const std::string* Element::GetAttribute(const std::string& name, int* i) const
{
	const std::string* s = GetAttribute(name);
	if (i)
	{
		if (s)
		{
			*i = atoi(s->c_str());
		}
		else
		{
			*i = 0;
		}
	}
	return s;
}

const char* Element::GetAttribute(const char* name, double* d) const
{
	const char* s = GetAttribute(name);
	if (d)
	{
		if (s)
		{
			*d = atof(s);
		}
		else
		{
			*d = 0;
		}
	}
	return s;
}

const std::string* Element::GetAttribute(const std::string& name, double* d) const
{
	const std::string* s = GetAttribute(name);
	if (d)
	{
		if (s)
		{
			*d = atof(s->c_str());
		}
		else
		{
			*d = 0;
		}
	}
	return s;
}


int Element::QueryIntAttribute(const char* name, int* ival) const
{
	const Attribute* node = attributeSet.Find(name);
	if (!node)
	{
		return TIXML_NO_ATTRIBUTE;
	}
	return node->QueryIntValue(ival);
}

int Element::QueryIntAttribute(const std::string& name, int* ival) const
{
	const Attribute* node = attributeSet.Find(name);
	if (!node)
	{
		return TIXML_NO_ATTRIBUTE;
	}
	return node->QueryIntValue(ival);
}


int Element::QueryDoubleAttribute(const char* name, double* dval) const
{
	const Attribute* node = attributeSet.Find(name);
	if (!node)
	{
		return TIXML_NO_ATTRIBUTE;
	}
	return node->QueryDoubleValue(dval);
}

int Element::QueryDoubleAttribute(const std::string& name, double* dval) const
{
	const Attribute* node = attributeSet.Find(name);
	if (!node)
	{
		return TIXML_NO_ATTRIBUTE;
	}
	return node->QueryDoubleValue(dval);
}


void Element::SetAttribute(const char* name, int val)
{
	char buf[64];
#if defined(TIXML_SNPRINTF)
	TIXML_SNPRINTF(buf, sizeof(buf), "%d", val);
#else
	sprintf( buf, "%d", val );
#endif
	SetAttribute(name, buf);
}

void Element::SetAttribute(const std::string& name, int val)
{
	std::ostringstream oss;
	oss << val;
	SetAttribute(name, oss.str());
}


void Element::SetDoubleAttribute(const char* name, double val)
{
	char buf[256];
#if defined(TIXML_SNPRINTF)
	TIXML_SNPRINTF(buf, sizeof(buf), "%f", val);
#else
	sprintf( buf, "%f", val );
#endif
	SetAttribute(name, buf);
}


void Element::SetAttribute(const char* cname, const char* cvalue)
{

	std::string _name(cname);
	std::string _value(cvalue);

	Attribute* node = attributeSet.Find(_name);
	if (node)
	{
		node->SetValue(_value);
		return;
	}

	Attribute* attrib = new Attribute(cname, cvalue);
	if (attrib)
	{
		attributeSet.Add(attrib);
	}
	else
	{
		Document* document = GetDocument();
		if (document)
		{ document->SetError(TIXML_ERROR_OUT_OF_MEMORY, 0, 0, Encoding::Unknow); }
	}
}

void Element::SetAttribute(const std::string& name, const std::string& _value)
{
	Attribute* node = attributeSet.Find(name);
	if (node)
	{
		node->SetValue(_value);
		return;
	}

	Attribute* attrib = new Attribute(name, _value);
	if (attrib)
	{
		attributeSet.Add(attrib);
	}
	else
	{
		Document* document = GetDocument();
		if (document)
		{ document->SetError(TIXML_ERROR_OUT_OF_MEMORY, 0, 0, Encoding::Unknow); }
	}
}


void Element::Print(FILE* cfile, int depth) const
{
	int i;
	assert(cfile);
	for (i = 0; i < depth; i++)
	{
		fprintf(cfile, "    ");
	}

	fprintf(cfile, "<%s", value.c_str());

	const Attribute* attrib;
	for (attrib = attributeSet.First(); attrib; attrib = attrib->Next())
	{
		fprintf(cfile, " ");
		attrib->Print(cfile, depth);
	}

	// There are 3 different formatting approaches:
	// 1) An element without children is printed as a <foo /> node
	// 2) An element with only a text child is printed as <foo> text </foo>
	// 3) An element with children is printed on multiple lines.
	TiXmlNode* node;
	if (!firstChild)
	{
		fprintf(cfile, " />");
	}
	else if (firstChild == lastChild && firstChild->ToText())
	{
		fprintf(cfile, ">");
		firstChild->Print(cfile, depth + 1);
		fprintf(cfile, "</%s>", value.c_str());
	}
	else
	{
		fprintf(cfile, ">");

		for (node = firstChild; node; node = node->NextSibling())
		{
			if (!node->ToText())
			{
				fprintf(cfile, "\n");
			}
			node->Print(cfile, depth + 1);
		}
		fprintf(cfile, "\n");
		for (i = 0; i < depth; ++i)
		{
			fprintf(cfile, "    ");
		}
		fprintf(cfile, "</%s>", value.c_str());
	}
}


void Element::CopyTo(Element* target) const
{
	// superclass:
	TiXmlNode::CopyTo(target);

	// Element class:
	// Clone the attributes, then clone the children.
	const Attribute* attribute = 0;
	for (attribute = attributeSet.First();
		 attribute;
		 attribute = attribute->Next())
	{
		target->SetAttribute(attribute->Name(), attribute->Value());
	}

	TiXmlNode* node = 0;
	for (node = firstChild; node; node = node->NextSibling())
	{
		target->LinkEndChild(node->Clone());
	}
}

bool Element::Accept(TiXmlVisitor* visitor) const
{
	if (visitor->VisitEnter(*this, attributeSet.First()))
	{
		for (const TiXmlNode* node = FirstChild(); node; node = node->NextSibling())
		{
			if (!node->Accept(visitor))
			{
				break;
			}
		}
	}
	return visitor->VisitExit(*this);
}


TiXmlNode* Element::Clone() const
{
	Element* clone = new Element(GetValue());
	if (!clone)
	{
		return 0;
	}

	CopyTo(clone);
	return clone;
}


const char* Element::GetText() const
{
	const TiXmlNode* child = this->FirstChild();
	if (child)
	{
		const Text* childText = child->ToText();
		if (childText)
		{
			return childText->GetValue();
		}
	}
	return 0;
}


Document::Document() : TiXmlNode(TiXmlNode::DOCUMENT)
{
	tabsize = 4;
	useMicrosoftBOM = false;
	ClearError();
}

Document::Document(const char* documentName) : TiXmlNode(TiXmlNode::DOCUMENT)
{
	tabsize = 4;
	useMicrosoftBOM = false;
	value = documentName;
	ClearError();
}

Document::Document(const std::string& documentName) : TiXmlNode(TiXmlNode::DOCUMENT)
{
	tabsize = 4;
	useMicrosoftBOM = false;
	value = documentName;
	ClearError();
}


Document::Document(const Document& copy) : TiXmlNode(TiXmlNode::DOCUMENT)
{
	copy.CopyTo(this);
}


void Document::operator=(const Document& copy)
{
	Clear();
	copy.CopyTo(this);
}


bool Document::LoadFile(Encoding encoding)
{
	// See STL_STRING_BUG below.
	//StringToBuffer buf( value );

	return LoadFile(GetValue(), encoding);
}


bool Document::SaveFile() const
{
	// See STL_STRING_BUG below.
//	StringToBuffer buf( value );
//
//	if ( buf.buffer && SaveFile( buf.buffer ) )
//		return true;
//
//	return false;
	return SaveFile(GetValue());
}

bool Document::LoadFile(const char* _filename, Encoding encoding)
{
	// There was a really terrifying little bug here. The code:
	//		value = filename
	// in the STL case, cause the assignment method of the std::string to
	// be called. What is strange, is that the std::string had the same
	// address as it's c_str() method, and so bad things happen. Looks
	// like a bug in the Microsoft STL implementation.
	// Add an extra string to avoid the crash.
	std::string filename(_filename);
	filename = filename;

	// reading in binary mode so that tinyxml can normalize the EOL
	FILE* file = TiXmlFOpen(filename.c_str(), "rb");

	if (file)
	{
		bool result = LoadFile(file, encoding);
		fclose(file);
		return result;
	}
	else
	{
		SetError(TIXML_ERROR_OPENING_FILE, 0, 0, Encoding::Unknow);
		return false;
	}
}

bool Document::LoadFile(FILE* file, Encoding encoding)
{
	if (!file)
	{
		SetError(TIXML_ERROR_OPENING_FILE, 0, 0, Encoding::Unknow);
		return false;
	}

	// Delete the existing data:
	Clear();
	location.Clear();

	// Get the file size, so we can pre-allocate the string. HUGE speed impact.
	long length = 0;
	fseek(file, 0, SEEK_END);
	length = ftell(file);
	fseek(file, 0, SEEK_SET);

	// Strange case, but good to handle up front.
	if (length <= 0)
	{
		SetError(TIXML_ERROR_DOCUMENT_EMPTY, 0, 0, Encoding::Unknow);
		return false;
	}

	// If we have a file, assume it is all one big XML file, and read it in.
	// The document parser may decide the document ends sooner than the entire file, however.
	std::string data;
	data.reserve(length);

	// Subtle bug here. TinyXml did use fgets. But from the XML spec:
	// 2.11 End-of-Line Handling
	// <snip>
	// <quote>
	// ...the XML processor MUST behave as if it normalized all line breaks in external
	// parsed entities (including the document entity) on input, before parsing, by translating
	// both the two-character sequence #xD #xA and any #xD that is not followed by #xA to
	// a single #xA character.
	// </quote>
	//
	// It is not clear fgets does that, and certainly isn't clear it works cross platform.
	// Generally, you expect fgets to translate from the convention of the OS to the c/unix
	// convention, and not work generally.

	/*
	while( fgets( buf, sizeof(buf), file ) )
	{
		data += buf;
	}
	*/

	char* buf = new char[length + 1];
	buf[0] = 0;

	if (fread(buf, length, 1, file) != 1)
	{
		delete[] buf;
		SetError(TIXML_ERROR_OPENING_FILE, 0, 0, Encoding::Unknow);
		return false;
	}

	const char* lastPos = buf;
	const char* p = buf;

	buf[length] = 0;
	while (*p)
	{
		assert(p < (buf + length));
		if (*p == 0xa)
		{
			// Newline character. No special rules for this. Append all the characters
			// since the last string, and include the newline.
			data.append(lastPos, (p - lastPos + 1));    // append, include the newline
			++p;                                    // move past the newline
			lastPos = p;                            // and point to the new buffer (may be 0)
			assert(p <= (buf + length));
		}
		else if (*p == 0xd)
		{
			// Carriage return. Append what we have so far, then
			// handle moving forward in the buffer.
			if ((p - lastPos) > 0)
			{
				data.append(lastPos, p - lastPos);    // do not add the CR
			}
			data += (char)0xa;                        // a proper newline

			if (*(p + 1) == 0xa)
			{
				// Carriage return - new line sequence
				p += 2;
				lastPos = p;
				assert(p <= (buf + length));
			}
			else
			{
				// it was followed by something else...that is presumably characters again.
				++p;
				lastPos = p;
				assert(p <= (buf + length));
			}
		}
		else
		{
			++p;
		}
	}
	// Handle any left over characters.
	if (p - lastPos)
	{
		data.append(lastPos, p - lastPos);
	}
	delete[] buf;
	buf = 0;

	Parse(data.c_str(), 0, encoding);

	if (Error())
	{
		return false;
	}
	else
	{
		return true;
	}
}


bool Document::SaveFile(const char* filename) const
{
	// The old c stuff lives on...
	FILE* fp = TiXmlFOpen(filename, "w");
	if (fp)
	{
		bool result = SaveFile(fp);
		fclose(fp);
		return result;
	}
	return false;
}


bool Document::SaveFile(FILE* fp) const
{
	if (useMicrosoftBOM)
	{
		const unsigned char TIXML_UTF_LEAD_0 = 0xefU;
		const unsigned char TIXML_UTF_LEAD_1 = 0xbbU;
		const unsigned char TIXML_UTF_LEAD_2 = 0xbfU;

		fputc(TIXML_UTF_LEAD_0, fp);
		fputc(TIXML_UTF_LEAD_1, fp);
		fputc(TIXML_UTF_LEAD_2, fp);
	}
	Print(fp, 0);
	return (ferror(fp) == 0);
}


void Document::CopyTo(Document* target) const
{
	TiXmlNode::CopyTo(target);

	target->error = error;
	target->errorId = errorId;
	target->errorDesc = errorDesc;
	target->tabsize = tabsize;
	target->errorLocation = errorLocation;
	target->useMicrosoftBOM = useMicrosoftBOM;

	TiXmlNode* node = 0;
	for (node = firstChild; node; node = node->NextSibling())
	{
		target->LinkEndChild(node->Clone());
	}
}


TiXmlNode* Document::Clone() const
{
	Document* clone = new Document();
	if (!clone)
	{
		return 0;
	}

	CopyTo(clone);
	return clone;
}


void Document::Print(FILE* cfile, int depth) const
{
	assert(cfile);
	for (const TiXmlNode* node = FirstChild(); node; node = node->NextSibling())
	{
		node->Print(cfile, depth);
		fprintf(cfile, "\n");
	}
}


bool Document::Accept(TiXmlVisitor* visitor) const
{
	if (visitor->VisitEnter(*this))
	{
		for (const TiXmlNode* node = FirstChild(); node; node = node->NextSibling())
		{
			if (!node->Accept(visitor))
			{
				break;
			}
		}
	}
	return visitor->VisitExit(*this);
}


const Attribute* Attribute::Next() const
{
	// We are using knowledge of the sentinel. The sentinel
	// have a value or name.
	if (next->value.empty() && next->name.empty())
	{
		return 0;
	}
	return next;
}

/*
Attribute* Attribute::Next()
{
	// We are using knowledge of the sentinel. The sentinel
	// have a value or name.
	if ( next->value.empty() && next->name.empty() )
		return 0;
	return next;
}
*/

const Attribute* Attribute::Previous() const
{
	// We are using knowledge of the sentinel. The sentinel
	// have a value or name.
	if (prev->value.empty() && prev->name.empty())
	{
		return 0;
	}
	return prev;
}

/*
Attribute* Attribute::Previous()
{
	// We are using knowledge of the sentinel. The sentinel
	// have a value or name.
	if ( prev->value.empty() && prev->name.empty() )
		return 0;
	return prev;
}
*/

void Attribute::Print(FILE* cfile, int /*depth*/, std::string* str) const
{
	std::string n, v;

	EncodeString(name, &n);
	EncodeString(value, &v);

	if (value.find('\"') == std::string::npos)
	{
		if (cfile)
		{
			fprintf(cfile, "%s=\"%s\"", n.c_str(), v.c_str());
		}
		if (str)
		{
			(*str) += n;
			(*str) += "=\"";
			(*str) += v;
			(*str) += "\"";
		}
	}
	else
	{
		if (cfile)
		{
			fprintf(cfile, "%s='%s'", n.c_str(), v.c_str());
		}
		if (str)
		{
			(*str) += n;
			(*str) += "='";
			(*str) += v;
			(*str) += "'";
		}
	}
}


int Attribute::QueryIntValue(int* ival) const
{
	if (TIXML_SSCANF(value.c_str(), "%d", ival) == 1)
	{
		return TIXML_SUCCESS;
	}
	return TIXML_WRONG_TYPE;
}

int Attribute::QueryDoubleValue(double* dval) const
{
	if (TIXML_SSCANF(value.c_str(), "%lf", dval) == 1)
	{
		return TIXML_SUCCESS;
	}
	return TIXML_WRONG_TYPE;
}

void Attribute::SetIntValue(int _value)
{
	char buf[64];
#if defined(TIXML_SNPRINTF)
	TIXML_SNPRINTF(buf, sizeof(buf), "%d", _value);
#else
	sprintf (buf, "%d", _value);
#endif
	SetValue(buf);
}

void Attribute::SetDoubleValue(double _value)
{
	char buf[256];
#if defined(TIXML_SNPRINTF)
	TIXML_SNPRINTF(buf, sizeof(buf), "%lf", _value);
#else
	sprintf (buf, "%lf", _value);
#endif
	SetValue(buf);
}

int Attribute::IntValue() const
{
	return atoi(value.c_str());
}

double Attribute::DoubleValue() const
{
	return atof(value.c_str());
}


Comment::Comment(const Comment& copy) : TiXmlNode(TiXmlNode::COMMENT)
{
	copy.CopyTo(this);
}


void Comment::operator=(const Comment& base)
{
	Clear();
	base.CopyTo(this);
}


void Comment::Print(FILE* cfile, int depth) const
{
	assert(cfile);
	for (int i = 0; i < depth; i++)
	{
		fprintf(cfile, "    ");
	}
	fprintf(cfile, "<!--%s-->", value.c_str());
}


void Comment::CopyTo(Comment* target) const
{
	TiXmlNode::CopyTo(target);
}


bool Comment::Accept(TiXmlVisitor* visitor) const
{
	return visitor->Visit(*this);
}


TiXmlNode* Comment::Clone() const
{
	Comment* clone = new Comment();

	if (!clone)
	{
		return 0;
	}

	CopyTo(clone);
	return clone;
}


void Text::Print(FILE* cfile, int depth) const
{
	assert(cfile);
	if (cdata)
	{
		int i;
		fprintf(cfile, "\n");
		for (i = 0; i < depth; i++)
		{
			fprintf(cfile, "    ");
		}
		fprintf(cfile, "<![CDATA[%s]]>\n", value.c_str());    // unformatted output
	}
	else
	{
		std::string buffer;
		EncodeString(value, &buffer);
		fprintf(cfile, "%s", buffer.c_str());
	}
}


void Text::CopyTo(Text* target) const
{
	TiXmlNode::CopyTo(target);
	target->cdata = cdata;
}


bool Text::Accept(TiXmlVisitor* visitor) const
{
	return visitor->Visit(*this);
}


TiXmlNode* Text::Clone() const
{
	Text* clone = 0;
	clone = new Text("");

	if (!clone)
	{
		return 0;
	}

	CopyTo(clone);
	return clone;
}


Declaration::Declaration(const char* _version,
		const char* _encoding,
		const char* _standalone)
		: TiXmlNode(TiXmlNode::DECLARATION)
{
	version = _version;
	encoding = _encoding;
	standalone = _standalone;
}

Declaration::Declaration(const std::string& _version,
		const std::string& _encoding,
		const std::string& _standalone)
		: TiXmlNode(TiXmlNode::DECLARATION)
{
	version = _version;
	encoding = _encoding;
	standalone = _standalone;
}


Declaration::Declaration(const Declaration& copy)
		: TiXmlNode(TiXmlNode::DECLARATION)
{
	copy.CopyTo(this);
}


void Declaration::operator=(const Declaration& copy)
{
	Clear();
	copy.CopyTo(this);
}


void Declaration::Print(FILE* cfile, int /*depth*/, std::string* str) const
{
	if (cfile)
	{ fprintf(cfile, "<?xml "); }
	if (str)
	{ (*str) += "<?xml "; }

	if (!version.empty())
	{
		if (cfile)
		{ fprintf(cfile, "version=\"%s\" ", version.c_str()); }
		if (str)
		{
			(*str) += "version=\"";
			(*str) += version;
			(*str) += "\" ";
		}
	}
	if (!encoding.empty())
	{
		if (cfile)
		{ fprintf(cfile, "encoding=\"%s\" ", encoding.c_str()); }
		if (str)
		{
			(*str) += "encoding=\"";
			(*str) += encoding;
			(*str) += "\" ";
		}
	}
	if (!standalone.empty())
	{
		if (cfile)
		{ fprintf(cfile, "standalone=\"%s\" ", standalone.c_str()); }
		if (str)
		{
			(*str) += "standalone=\"";
			(*str) += standalone;
			(*str) += "\" ";
		}
	}
	if (cfile)
	{ fprintf(cfile, "?>"); }
	if (str)
	{ (*str) += "?>"; }
}


void Declaration::CopyTo(Declaration* target) const
{
	TiXmlNode::CopyTo(target);

	target->version = version;
	target->encoding = encoding;
	target->standalone = standalone;
}


bool Declaration::Accept(TiXmlVisitor* visitor) const
{
	return visitor->Visit(*this);
}


TiXmlNode* Declaration::Clone() const
{
	Declaration* clone = new Declaration();

	if (!clone)
	{
		return 0;
	}

	CopyTo(clone);
	return clone;
}

StylesheetReference::StylesheetReference(const char* _type,
		const char* _href)
		: TiXmlNode(TiXmlNode::STYLESHEETREFERENCE)
{
	type = _type;
	href = _href;
}

StylesheetReference::StylesheetReference(const std::string& _type,
		const std::string& _href)
		: TiXmlNode(TiXmlNode::STYLESHEETREFERENCE)
{
	type = _type;
	href = _href;
}


StylesheetReference::StylesheetReference(const StylesheetReference& copy)
		: TiXmlNode(TiXmlNode::STYLESHEETREFERENCE)
{
	copy.CopyTo(this);
}


void StylesheetReference::operator=(const StylesheetReference& copy)
{
	Clear();
	copy.CopyTo(this);
}


void StylesheetReference::Print(FILE* cfile, int /*depth*/, std::string* str) const
{
	if (cfile)
	{ fprintf(cfile, "<?xml-stylesheet "); }
	if (str)
	{ (*str) += "<?xml-stylesheet "; }

	if (!type.empty())
	{
		if (cfile)
		{ fprintf(cfile, "type=\"%s\" ", type.c_str()); }
		if (str)
		{
			(*str) += "type=\"";
			(*str) += type;
			(*str) += "\" ";
		}
	}
	if (!href.empty())
	{
		if (cfile)
		{ fprintf(cfile, "href=\"%s\" ", href.c_str()); }
		if (str)
		{
			(*str) += "href=\"";
			(*str) += href;
			(*str) += "\" ";
		}
	}
	if (cfile)
	{ fprintf(cfile, "?>"); }
	if (str)
	{ (*str) += "?>"; }
}

void StylesheetReference::CopyTo(StylesheetReference* target) const
{
	TiXmlNode::CopyTo(target);

	target->type = type;
	target->href = href;
}

bool StylesheetReference::Accept(TiXmlVisitor* visitor) const
{
	return visitor->Visit(*this);
}

TiXmlNode* StylesheetReference::Clone() const
{
	StylesheetReference* clone = new StylesheetReference();

	if (!clone)
	{
		return 0;
	}

	CopyTo(clone);
	return clone;
}


void Unknown::Print(FILE* cfile, int depth) const
{
	for (int i = 0; i < depth; i++)
	{
		fprintf(cfile, "    ");
	}
	fprintf(cfile, "<%s>", value.c_str());
}


void Unknown::CopyTo(Unknown* target) const
{
	TiXmlNode::CopyTo(target);
}


bool Unknown::Accept(TiXmlVisitor* visitor) const
{
	return visitor->Visit(*this);
}


TiXmlNode* Unknown::Clone() const
{
	Unknown* clone = new Unknown();

	if (!clone)
	{
		return 0;
	}

	CopyTo(clone);
	return clone;
}


TiXmlAttributeSet::TiXmlAttributeSet()
{
	sentinel.next = &sentinel;
	sentinel.prev = &sentinel;
}


TiXmlAttributeSet::~TiXmlAttributeSet()
{
	assert(sentinel.next == &sentinel);
	assert(sentinel.prev == &sentinel);
}


void TiXmlAttributeSet::Add(Attribute* addMe)
{
	((!Find(std::string(addMe->Name()))) ? static_cast<void>(0) : __assert_fail("!Find(TIXML_STRING(addMe->Name()))",
			"_file_name_", 1795, "_function_name_"));    // Shouldn't be multiply adding to the set.

	addMe->next = &sentinel;
	addMe->prev = sentinel.prev;

	sentinel.prev->next = addMe;
	sentinel.prev = addMe;
}

void TiXmlAttributeSet::Remove(Attribute* removeMe)
{
	assert(removeMe);
	Attribute* node;

	for (node = sentinel.next; node != &sentinel; node = node->next)
	{
		if (node == removeMe)
		{
			node->prev->next = node->next;
			node->next->prev = node->prev;
			node->next = 0;
			node->prev = 0;
			return;
		}
	}
	assert(0);        // we tried to remove a non-linked attribute.
}

const Attribute* TiXmlAttributeSet::Find(const std::string& name) const
{
	for (const Attribute* node = sentinel.next; node != &sentinel; node = node->next)
	{
		if (node->name == name)
		{
			return node;
		}
	}
	return 0;
}

const Attribute* TiXmlAttributeSet::Find(const char* name) const
{
	for (const Attribute* node = sentinel.next; node != &sentinel; node = node->next)
	{
		if (strcmp(node->name.c_str(), name) == 0)
		{
			return node;
		}
	}
	return 0;
}

std::istream& operator>>(std::istream& in, TiXmlNode& base)
{
	std::string tag;
	tag.reserve(8 * 1000);
	base.StreamIn(&in, &tag);

	base.Parse(tag.c_str(), 0, DEFAULT_ENCODING);
	return in;
}

std::ostream& operator<<(std::ostream& out, const TiXmlNode& base)
{
	TiXmlPrinter printer;
	printer.SetStreamPrinting();
	base.Accept(&printer);
	out << printer.Str();

	return out;
}


std::string& operator<<(std::string& out, const TiXmlNode& base)
{
	TiXmlPrinter printer;
	printer.SetStreamPrinting();
	base.Accept(&printer);
	out.append(printer.Str());

	return out;
}